<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use AppBundle\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DashboardController extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction()
    {
        $repository = $this->getDoctrine()->getRepository(Post::class);
        $allPosts = $repository->findAll();
        
        return $this->render('AppBundle:Dashboard:mid_dashboard.html.twig', array(
            'allPosts' => $allPosts,
        ));
    }
    
    
    /**
     * @Route("/charts", name="charts")
     */
    public function chartsAction()
    {
        return $this->render('AppBundle:Charts:mid_charts.html.twig', array(
            // ...
        ));
    }    

    
    /**
     * @Route("/news", name="news")
     */
    public function newsAction()
    {
        return $this->render('AppBundle:News:mid_news.html.twig', array(
            // ...
        ));
    }
    
    
    /**
     * @Route("/tables", name="tables")
     */
    public function tablesAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Post::class);
        
        if ($request->isMethod('POST')) {
            
            $em = $this->getDoctrine()->getManager();
            
            $post = new Post();
            
            $title = $request->request->get('title');
            $text  = $request->request->get('text');
            
            $post->setTitle($title);
            $post->setText($text);
            
            $em->persist($post);
            $em->flush();
            
            dump($post);
        }
        
        $allPosts = $repository->findAll();        
        
        $post = new Post();
        
        $form = $this->createFormBuilder($post)
            ->add('title', TextType::class)
            ->add('text', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();
            
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $task = $form->getData();
            
            dump($task);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();            
            

            return $this->redirect('http://new_theme/app_dev.php/tables');
        } 
        
        dump($form->handleRequest($request));
        
        return $this->render('AppBundle:Tables:mid_tables.html.twig', array(
            'allPosts' => $allPosts,
        ));
    }
}
